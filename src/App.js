import { useState, useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";

// components
import AppNavbar from "./components/AppNavbar";

// pages
import Post from "./pages/Post";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";

// css
import "./App.css";

// User context - saves user details
import { UserProvider } from "./UserContext";

function App() {
  const [user, setUser] = useState({
    id: null,
    firstName: null,
    isAdmin: null,
  });
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    if (localStorage.getItem("token")) {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUser({
            id: data._id,
            firstName: data.firstName,
            isAdmin: data.isAdmin,
          });
        })
        .catch((error) => {});
    }
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Routes>
          <Route path="/" element={<Post />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/register" element={<Register />} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
