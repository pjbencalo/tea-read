import { useContext } from "react";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import UserContext from "../UserContext";
import { Book, Cup } from "react-bootstrap-icons";

function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar
      expand="lg"
      className="bg-body-tertiary fixed-top dancing-script"
      bg="dark"
      data-bs-theme="dark"
    >
      <Container>
        <Navbar.Brand as={NavLink} to="/">
          <Cup size={24} className="me-2" />
          Tea-Read
          <Book size={24} className="ms-2" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/">
              Post
            </Nav.Link>

            <NavDropdown title="My Account" id="basic-nav-dropdown">
              {user.id ? (
                <NavDropdown.Item as={Link} to="/logout">
                  Logout
                </NavDropdown.Item>
              ) : (
                <>
                  <NavDropdown.Item as={Link} to="/login">
                    Login
                  </NavDropdown.Item>
                  <NavDropdown.Item as={Link} to="/register">
                    Register
                  </NavDropdown.Item>
                </>
              )}
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AppNavbar;
