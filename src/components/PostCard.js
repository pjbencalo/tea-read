import { useContext, useEffect, useState } from "react";
import { Button, Col, Dropdown, Form, Modal } from "react-bootstrap";
import { ThreeDotsVertical, HeartFill } from "react-bootstrap-icons";
import UserContext from "../UserContext";
import { DateTime } from "luxon";
import Swal from "sweetalert2";

export default function PostCard({ post, onPostDelete, onPostUpdate }) {
  const { user } = useContext(UserContext);
  const [show, setShow] = useState(false);
  const [updatePostId, setUpdatePostId] = useState("");
  const [updateBody, setUpdateBody] = useState("");
  const [hasLiked, setHasLiked] = useState(false);

  const handleClose = () => setShow(false);

  const handleShow = (post) => {
    setUpdatePostId(post._id);
    setUpdateBody(post.body);
    setShow(true);
  };

  useEffect(() => {
    setHasLiked(post.likes.includes(user.id));
  }, [post.likes, user.id]);

  const likePost = (postId) => {
    fetch(`${process.env.REACT_APP_API_URL}/posts/${postId}/like`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setHasLiked(true);
        onPostUpdate(data);
      });
  };

  const unlikePost = (postId) => {
    fetch(`${process.env.REACT_APP_API_URL}/posts/${postId}/unlike`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setHasLiked(false);
        onPostUpdate(data);
      });
  };

  const updatePost = () => {
    fetch(`${process.env.REACT_APP_API_URL}/posts/${updatePostId}/update`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        body: updateBody,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        onPostUpdate(data);
        Swal.fire({
          icon: "success",
          title: "Post updated",
        });
        setShow(false);
      })
      .catch((err) => {});
  };

  const deletePost = (postId) => {
    fetch(`${process.env.REACT_APP_API_URL}/posts/${postId}/archive`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        onPostDelete(postId);
        Swal.fire({
          icon: "success",
          title: "Post deleted",
        });
      })
      .catch((err) => {});
  };

  return (
    <Col
      xs={12}
      md={7}
      data-aos="fade-zoom-in"
      data-aos-easing="ease-in-back"
      data-aos-delay="100"
      data-aos-offset="0"
    >
      <Col className="border p-3 my-1 glass-effect">
        <Col className="d-flex">
          <h5 className="me-2 bold dancing-script">{post.author}</h5>
          <p className="mx-2"> • </p>
          <p className="ms-2">
            {DateTime.fromISO(post.createdOn).toRelative()}
          </p>
          {user.id === post.authorId ? (
            <>
              <Dropdown className="ms-auto" bg="dark" data-bs-theme="dark">
                <Dropdown.Toggle
                  id="dropdown-basic"
                  className="custom-toggle"
                  style={{ background: "none", border: "none", padding: 0 }}
                >
                  <ThreeDotsVertical className="dropdown-icon" />
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => handleShow(post)}
                    className="dropdown-item"
                  >
                    Update
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => deletePost(post._id)}
                    className="dropdown-item"
                  >
                    Delete
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </>
          ) : (
            <Dropdown className="ms-auto" bg="dark" data-bs-theme="dark">
              <Dropdown.Toggle
                id="dropdown-basic"
                className="custom-toggle"
                style={{ background: "none", border: "none", padding: 0 }}
              >
                <ThreeDotsVertical className="dropdown-icon" />
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item className="dropdown-item">Share</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          )}
        </Col>
        <Col>
          <p>{post.body}</p>
        </Col>
        <Col className="d-flex">
          {hasLiked ? (
            <HeartFill
              className="me-2"
              size={24}
              fill="red"
              cursor="pointer"
              onClick={() => unlikePost(post._id)}
            />
          ) : (
            <HeartFill
              className="me-2"
              size={24}
              cursor="pointer"
              onClick={() => likePost(post._id)}
            />
          )}
          {post.likes.length} likes
        </Col>
      </Col>

      <Modal show={show} onHide={handleClose} dialogClassName="dark-modal">
        <Modal.Header closeButton>
          <Modal.Title className="dancing-script">Update Post</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Control
            type="text"
            value={updateBody}
            onChange={(e) => setUpdateBody(e.target.value)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            className="dancing-script"
            variant="secondary"
            onClick={handleClose}
          >
            Close
          </Button>
          <Button
            className="dancing-script"
            variant="primary"
            onClick={updatePost}
          >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </Col>
  );
}
