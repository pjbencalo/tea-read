import { ReactComponent as EyeIcon } from "bootstrap-icons/icons/eye.svg";
import { ReactComponent as EyeSlashIcon } from "bootstrap-icons/icons/eye-slash.svg";
import { useContext, useEffect, useState } from "react";
import {
  Button,
  Card,
  Container,
  Col,
  Row,
  Form,
  InputGroup,
} from "react-bootstrap";
import { Link, Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Login() {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  function authenticate(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.access !== "undefined") {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);
          Swal.fire({
            icon: "success",
            title: "Login successful!",
            text: "Welcome to Tea-Read",
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Authentication failed",
            text: "Please check your login credentials and try again",
          });
        }
      })
      .catch((error) => {});
    setEmail("");
    setPassword("");
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          firstName: data.firstName,
          isAdmin: data.isAdmin,
        });
      })
      .catch((error) => {});
  };

  useEffect(() => {
    if (email !== "" && password != "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id ? (
    <Navigate to="/" />
  ) : (
    <Container>
      <Row className="min-vh-100 d-flex align-items-center justify-content-center">
        <Col xs={12} md={4}>
          <Card className="glass-effect">
            <Form className="p-3" onSubmit={(e) => authenticate(e)}>
              <Form.Group>
                <Form.Label className="text-center w-100">
                  <h2 className="white">Log In</h2>
                </Form.Label>
              </Form.Group>

              <Form.Group>
                <Form.Control
                  className="my-3"
                  type="email"
                  placeholder="Enter email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
              </Form.Group>

              <Form.Group>
                <InputGroup>
                  <Form.Control
                    type={showPassword ? "text" : "password"}
                    placeholder="Enter Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />

                  <InputGroup.Text
                    onClick={() => setShowPassword(!showPassword)}
                    style={{ cursor: "pointer" }}
                    className="border"
                  >
                    {showPassword ? <EyeSlashIcon /> : <EyeIcon />}
                  </InputGroup.Text>
                </InputGroup>
              </Form.Group>
              {isActive ? (
                <Button
                  type="submit"
                  className="my-3 w-100"
                  variant="secondary"
                >
                  Login
                </Button>
              ) : (
                <Button
                  type="submit"
                  className="my-3 w-100"
                  variant="secondary"
                  disabled
                >
                  Login
                </Button>
              )}
              <div className="w-100 text-center ">
                <Form.Text className="white">
                  Don't have an account yet?{" "}
                  <Link to="/register">Register here</Link>
                </Form.Text>
              </div>
            </Form>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
