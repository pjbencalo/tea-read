import { ReactComponent as EyeIcon } from "bootstrap-icons/icons/eye.svg";
import { ReactComponent as EyeSlashIcon } from "bootstrap-icons/icons/eye-slash.svg";
import { useState, useEffect, useContext } from "react";
import {
  Button,
  Card,
  Container,
  Col,
  Row,
  Form,
  InputGroup,
} from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Register() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  const [isActive, setIsActive] = useState(false);

  const [showPassword1, setShowPassword1] = useState(false);
  const [showPassword2, setShowPassword2] = useState(false);

  function registerUser(e) {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: "success",
            title: "Registered Complete",
            text: "Log in now.",
          });
          navigate("/login");
        } else {
          Swal.fire({
            icon: "error",
            title: "Duplicate Email",
            text: "Please provide another email",
          });
        }
      })
      .catch((error) => {});

    setFirstName("");
    setLastName("");
    setEmail("");
    setPassword1("");
    setPassword2("");
  }

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, password1, password2]);

  return user.id ? (
    <Navigate to="/" />
  ) : (
    <Container>
      <Row className="min-vh-100 align-items-center justify-content-center">
        <Col xs={12} md={4}>
          <Card className="glass-effect">
            <Form className="p-3" onSubmit={(e) => registerUser(e)}>
              <Form.Group controlId="userFirstName">
                <Form.Label className="text-center w-100">
                  <h2 className="white">Register</h2>
                </Form.Label>
              </Form.Group>
              <Form.Group>
                <Form.Control
                  className="my-3"
                  type="text"
                  placeholder="Enter first name"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                  required
                />
              </Form.Group>
              <Form.Group controlId="userLastName">
                <Form.Control
                  className="my-3"
                  type="text"
                  placeholder="Enter last name"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                  required
                />
              </Form.Group>
              <Form.Group controlId="userEmail">
                <Form.Control
                  className="my-3"
                  type="email"
                  placeholder="Enter email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
              </Form.Group>

              <Form.Group controlId="password1">
                <InputGroup className="my-3">
                  <Form.Control
                    type={showPassword1 ? "text" : "password"}
                    placeholder="Enter a password"
                    value={password1}
                    onChange={(e) => setPassword1(e.target.value)}
                    required
                  />
                  <InputGroup.Text
                    onClick={() => setShowPassword1(!showPassword1)}
                    style={{ cursor: "pointer" }}
                    className="border"
                  >
                    {showPassword1 ? <EyeSlashIcon /> : <EyeIcon />}
                  </InputGroup.Text>
                </InputGroup>
              </Form.Group>

              <Form.Group controlId="password2">
                <InputGroup>
                  <Form.Control
                    type={showPassword2 ? "text" : "password"}
                    placeholder="Confirm your password"
                    value={password2}
                    onChange={(e) => setPassword2(e.target.value)}
                    required
                  />
                  <InputGroup.Text
                    onClick={() => setShowPassword2(!showPassword2)}
                    style={{ cursor: "pointer" }}
                  >
                    {showPassword2 ? <EyeSlashIcon /> : <EyeIcon />}
                  </InputGroup.Text>
                </InputGroup>
              </Form.Group>

              {isActive ? (
                <Button
                  type="submit"
                  id="submit-button"
                  className="my-3 w-100"
                  variant="secondary"
                >
                  Register
                </Button>
              ) : (
                <Button
                  type="submit"
                  id="submit-button"
                  className="my-3 w-100"
                  variant="secondary"
                  disabled
                >
                  Register
                </Button>
              )}
              <div className="w-100 text-center ">
                <Form.Text className="white">
                  Already have an account? <Link to="/login">Log in here</Link>
                </Form.Text>
              </div>
            </Form>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
