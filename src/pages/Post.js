import { useContext, useEffect, useState } from "react";
import { Button, Container, Form, InputGroup, Row } from "react-bootstrap";
import PostCard from "../components/PostCard.js";
import Swal from "sweetalert2";
import UserContext from "../UserContext.js";

export default function Post() {
  const { user } = useContext(UserContext);
  const [posts, setPosts] = useState([]);
  const [inputValue, setInputValue] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/posts/active`)
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          const orderedPosts = data.reverse();
          setPosts(orderedPosts);
          setIsLoading(false);
        }
      })
      .catch((err) => {
        setIsLoading(false);
      });
  }, []);

  const addPost = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/posts/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        author: user.firstName,
        authorId: user.id,
        body: inputValue,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setPosts((prevPosts) => [data, ...prevPosts]);
          setInputValue("");
          Swal.fire({
            icon: "success",
            title: "Added successfully",
          });
        }
      });
  };

  const onPostDelete = (postId) => {
    setPosts((prevPosts) => prevPosts.filter((post) => post._id !== postId));
  };

  const onPostUpdate = (data) => {
    setPosts((prevPosts) =>
      prevPosts.map((post) => {
        if (post._id === data._id) {
          return { ...post, body: data.body, likes: data.likes };
        }
        return post;
      })
    );
  };

  return (
    <Container className="mt-5">
      <Row className="d-flex justify-content-center">
        <Form onSubmit={(e) => addPost(e)} className="mt-4 col-12 col-md-7">
          <InputGroup className="mb-3">
            <Form.Control
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              placeholder="What's on your mind?"
              value={inputValue}
              onChange={(e) => setInputValue(e.target.value)}
              required
            />
            {user.id ? (
              <Button type="submit" variant="secondary">
                Post
              </Button>
            ) : (
              <Button type="submit" disabled variant="secondary">
                Post
              </Button>
            )}
          </InputGroup>
        </Form>
      </Row>
      <Row className="d-flex justify-content-center mb-3">
        {isLoading ? (
          <div>
            <img
              src="https://www.bubble-tea-supply.com/images/website_image/loading.gif"
              className="loader"
              alt="loader"
            />
          </div>
        ) : (
          posts.map((post) => (
            <PostCard
              key={post._id}
              post={post}
              onPostDelete={onPostDelete}
              onPostUpdate={onPostUpdate}
            />
          ))
        )}
      </Row>
    </Container>
  );
}
